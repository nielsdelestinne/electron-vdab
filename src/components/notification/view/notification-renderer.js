import electron from "electron";
import NotificationAutoHider from "../autohider/notification-auto-hider"
const remote = electron.remote;
const notificatonAutoHider = new NotificationAutoHider(window, remote.getCurrentWindow());

notificatonAutoHider.start();

document
    .getElementById("notification-close-btn")
    .addEventListener("click", () => {
        const window = remote.getCurrentWindow();
        window.close();
    }
);