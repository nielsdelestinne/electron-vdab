const VISIBILITY_IN_SECONDS = 5;

export default class NotificationAutoHider  {

    constructor(windowDOM, currentWindow) {
        this.windowDOM = windowDOM;
        this.currentWindow = currentWindow;
    }

    start(visibilityInSeconds) {
        const hideAfterXSeconds = visibilityInSeconds ? visibilityInSeconds * 1000 : VISIBILITY_IN_SECONDS * 1000;
        this.timer = this.windowDOM.setTimeout(this.hideNotification(), hideAfterXSeconds);
        console.log(this);
    }

    hideNotification() {
        return () => {
            this.windowDOM.clearTimeout(this.timer);
            this.currentWindow.close();
        };

    }

}