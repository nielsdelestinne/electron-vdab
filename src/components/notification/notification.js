import electron from "electron";
import NotificationStore from "./store/notification-store"

const NOTIFICATION_WIDTH = 500;
const NOTIFICATION_HEIGHT = 120;
const NOTIFICATION_MARGIN = 10;

export default class Notification {

    constructor() {
        this.notification = undefined;
    }

    createWindow() {
        const {BrowserWindow} = electron.remote;
        const {screen: electronScreen} = electron;
        const {leftOffset, topOffset} = this.getNotificationPosition(electronScreen);
        this.notification = new BrowserWindow(
            {
                width: NOTIFICATION_WIDTH,
                height: NOTIFICATION_HEIGHT,
                x: leftOffset,
                y: topOffset,
                frame: false,
                center: false
            });

        // and load the index.html of the app.
        this.notification.loadURL(`file://${__dirname}/view/notification.html`);

        //this.notification.webContents.openDevTools();

        NotificationStore.addNotification(this.notification);

        // Emitted when the window is closed.
        this.notification.on('closed', () => {
            NotificationStore.removeNotification(this.notification, Notification.rePositionNotifications);
            this.notification = undefined;
        })
    }

    getNotificationPosition(electronScreen) {
        const {width, height} = this.getMonitorSize(electronScreen);
        const leftOffset = width - NOTIFICATION_WIDTH - NOTIFICATION_MARGIN;
        const topOffset = height - (NOTIFICATION_HEIGHT * NotificationStore.getNumberOfNotifications()) - (NOTIFICATION_MARGIN * NotificationStore.getNumberOfNotifications());
        return {leftOffset, topOffset};
    }

    getMonitorSize(electronScreen) {
        return electronScreen.getPrimaryDisplay().workAreaSize;
    }

    static rePositionNotifications() {
        let index = 1;
        for(const notification of NotificationStore.getNotifications()) {
            const {screen: electronScreen} = electron;
            const {leftOffset, topOffset} = Notification.reCalculateNotificationsPosition(electronScreen, index);
            notification.setBounds(
                {
                    width: NOTIFICATION_WIDTH,
                    height: NOTIFICATION_HEIGHT,
                    x: leftOffset,
                    y: topOffset,
                    frame: false,
                    center: false
                },
                true
            );
            index++;
        }
    }

    static reCalculateNotificationsPosition(electronScreen, positionIndex) {
        const {width, height} = electronScreen.getPrimaryDisplay().workAreaSize;
        const leftOffset = width - NOTIFICATION_WIDTH - NOTIFICATION_MARGIN;
        const topOffset = height - (NOTIFICATION_HEIGHT * positionIndex) - (NOTIFICATION_MARGIN * positionIndex);
        return {leftOffset, topOffset};
    }


}