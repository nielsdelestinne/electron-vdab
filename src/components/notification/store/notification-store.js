let numberOfNotifications = 1;
let notifications = [];

export default class NotificationStore {

    static addNotification(notification) {
        notifications.push(notification);
        numberOfNotifications++;
    }

    static removeNotification(notification, methodToExecute) {
        const indexOf = notifications.indexOf(notification);
        if(indexOf > -1) {
            notifications.splice(indexOf, 1);
            numberOfNotifications--;
            methodToExecute();
        }
    }

    static getNotifications() {
        return notifications;
    }

    static getNumberOfNotifications() {
        return numberOfNotifications;
    }

}