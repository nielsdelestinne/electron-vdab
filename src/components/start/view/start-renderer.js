import Notification from "../../notification/notification";

const newWindowBtn = document.getElementById('notification-spawn-btn');
newWindowBtn.addEventListener('click', function (event) {
    const notification = new Notification();
    notification.createWindow();
});