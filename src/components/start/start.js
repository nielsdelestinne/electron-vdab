import electron from "electron";

const WINDOW_WIDTH = 500;
const WINDOW_HEIGHT = 1750;

export default class StartWindow {

    constructor() {
        this.mainWindow = undefined;
    }

    createWindow() {
        // Create the browser window.
        const {BrowserWindow} = electron;
        this.mainWindow = new BrowserWindow({width: WINDOW_WIDTH, height: WINDOW_HEIGHT, x:0, y:0});

        // and load the index.html of the app.
        this.mainWindow.loadURL(`file://${__dirname}/view/start.html`);

        // Open the DevTools.
        this.mainWindow.webContents.openDevTools();

        // Emitted when the window is closed.
        this.mainWindow.on('closed', function () {
            // Dereference the window object, usually you would store windows
            // in an array if your app supports multi windows, this is the time
            // when you should delete the corresponding element.
            this.mainWindow = undefined
        });

    }

    getWindow() {
        return this.mainWindow;
    }

}