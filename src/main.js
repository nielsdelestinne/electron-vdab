import electron from "electron";
import StartWindow from "./components/start/start";
const { app } = electron; // of --> const app = electron.app;

const startWindow = new StartWindow();

app.on('ready', startWindow.createWindow); // This method will be called when Electron has finished

app.on('window-all-closed', () => { // Quit when all windows are closed.
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {
    if (startWindow.getWindow() === undefined) {
        startWindow.createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
