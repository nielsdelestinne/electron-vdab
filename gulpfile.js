var gulp = require("gulp");
var babel = require("gulp-babel");
//var concat = require('gulp-concat');

gulp.task("transpile-main", function () {
    return gulp.src("src/main.js")
        .pipe(babel())
        .pipe(gulp.dest("dist"));
});

gulp.task("transpile-components", function () {
    return gulp.src("src/components/**/*.js")
        .pipe(babel())
        .pipe(gulp.dest("dist/components"));
});

gulp.task("copy-html", function() {
   return gulp.src("src/components/**/*.html")
       .pipe(gulp.dest("dist/components"));
});

gulp.task("copy-css", function() {
    return gulp.src("src/css/*.css")
        .pipe(gulp.dest("dist/css"));
});


gulp.task("build",["copy-html", "copy-css", "transpile-main", "transpile-components"]);