# electron-quick-start

**Opmerking Niels:**
to run:

- `npm install`
- `npm install gulp`
- `npm install gulp-concat`
- `npm start`

**Clone and run for a quick way to see an Electron in action.**

This is a minimal Electron application based on the [Quick Start Guide](http://electron.atom.io/docs/latest/tutorial/quick-start) within the Electron documentation.

**Use this app along with the [Electron API Demos](http://electron.atom.io/#get-started) app for API code examples to help you get started.**

A basic Electron application needs just these files:

- `package.json` - Points to the app's main file and lists its details and dependencies.
- `main.js` - Starts the app and creates a browser window to render HTML. This is the app's **main process**.
- `index.html` - A web page to render. This is the app's **renderer process**.

You can learn more about each of these components within the [Quick Start Guide](http://electron.atom.io/docs/latest/tutorial/quick-start).

## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone https://github.com/electron/electron-quick-start
# Go into the repository
cd electron-quick-start
# Install dependencies and run the app
npm install && npm start
```

Learn more about Electron and its API in the [documentation](http://electron.atom.io/docs/latest).


## Integration in IntelliJ
http://blog.jetbrains.com/webstorm/2016/05/getting-started-with-electron-in-webstorm/

### To enable coding assistance for the Electron APIs
Add github-electron.d.ts as a JavaScript library:<br/>
Go to Preferences | Languages and Frameworks | JavaScript | Libraries, click Download...<br/>
--> search for github-electron and download it.

### Running and debugging the main process
Kies C:\electron\electron-vdab\node_modules\electron-prebuilt\dist\electron.exe als node interpreter
Neem dan wel dist\main.js (dus uit de dist folder) als Javascript file (omdat we de ES6->5 transpiling nodig hebben)


#### License [CC0 (Public Domain)](LICENSE.md)
